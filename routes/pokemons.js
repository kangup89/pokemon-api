const express = require("express");
const router = express.Router();

const PokemonProcessor = require('../models/pokemon-processor');

router.get("/pokemon", async (req, res) => {
    const resp = PokemonProcessor.getPokemons();
    res.status(resp.getStatus()).json(resp);
});

router.get("/pokemon/:id", async (req, res) => {

    const id = parseInt(req.params.id);

    const resp = PokemonProcessor.getPokemonById(id);

    res.status(resp.getStatus()).json(resp);  
});

router.post('/pokemon/add', async (req, res) => {
  
    const newPokemon = {
      ...req.body
    }
    
    const resp = PokemonProcessor.addPokemon(newPokemon);

    res.status(resp.getStatus()).json(resp);  
});

router.put('/pokemon/update/:id', async (req, res) => {

    const id = parseInt(req.params.id);

    const resp = PokemonProcessor.updatePokemon(id, req.body);

    res.status(resp.getStatus()).json(resp);
});

router.delete('/pokemon/delete/:id', async (req, res) => {

    const id = parseInt(req.params.id);
    
    const resp = PokemonProcessor.deletePokemon(id);

    res.status(resp.getStatus()).json(resp);
});

module.exports = router;