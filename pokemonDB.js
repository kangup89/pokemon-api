let pokemons = [
    {
        _id : 1,
        pokemonId: 1,
        name: "Bulbasaur",
        species: "Seed Pokémon",
        height: "0.7 m",
        weight: "6.9 kg",
        abilities: ["Overgrow", "Chlorophyll"],
        type: ["Grass", "Poison"],
        sprites: [{
            url: "https://img.pokemondb.net/sprites/x-y/shiny/bulbasaur.png",
            name: "Shiny"
        },{
            url: "https://img.pokemondb.net/sprites/x-y/normal/bulbasaur.png",
            name: "Normal"
        }],
        baseStats: {
            HP: 45,
            Attack: 49,
            Defense: 49,
            Sp_Atk: 65,
            Sp_Def: 65,
            Speed: 45,
            Total: 318
        }
    },
    {
        _id : 2,
        pokemonId: 172,
        name: "Pichu",
        species: "Tiny Mouse Pokémon",
        height: "0.3 m",
        weight: "2.0 kg",
        abilities: ["Static", "Lightning Rod"],
        type: ["ELECTRIC"],
        sprites: [{
            url: "https://img.pokemondb.net/sprites/sun-moon/shiny/pichu.png",
            name: "Shiny"
        },{
            url: "https://img.pokemondb.net/sprites/sun-moon/normal/pichu.png",
            name: "Normal"
        }],
        baseStats: {
            HP: 20,
            Attack: 40,
            Defense: 15,
            Sp_Atk: 35,
            Sp_Def: 35,
            Speed: 60,
            Total: 205
        }
    }
];

module.exports = pokemons;
