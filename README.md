Pokemon Api
====

## Endpoints
* [ GET ] /api/v1/pokemon
* [ GET ] /api/v1/pokemon/:id
* [ POST ] /api/v1/pokemon/add
* [ PUT ] /api/v1/pokemon/update/:id
* [ DELETE ] /api/v1/pokemon/delete/:id