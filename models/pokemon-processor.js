const customResponse = require('./custom-response');

class PokemonProcessor {
    constructor() {
        this.pokemons = require('../pokemonDB');
        this.count = 2;
    }

    getPokemons() {
        const resp = new customResponse();
        resp.setData(this.pokemons);
        resp.setSuccess(true);
        resp.setStatus(200);
        
        return resp;
    }

    getPokemonById(id) {
        const pokemon = this.pokemons.find(pokemon => pokemon._id === id);
        
        const resp = new customResponse();
        
        if(pokemon) {
            resp.setData(pokemon);
            resp.setSuccess(true);
            resp.setStatus(200);
        } else {
            resp.setSuccess(false);
            resp.setMessage("Pokemon not found");
            resp.setStatus(404);
        }
        
        return resp;
    }

    addPokemon(newPokemon) {
        const resp = new customResponse();

        if(!newPokemon.pokemonId) {
            resp.setSuccess(false);
            resp.setMessage('pokemonId is required'); 
            resp.setStatus(400);

            return resp;
        } else if(!newPokemon.name) {
            resp.setSuccess(false);
            resp.setMessage('name is required');
            resp.setStatus(400);

            return resp;
        }

        let p;

        this.pokemons.map((poke, index) => {
            if(poke.pokemonId === newPokemon.pokemonId) {
                p = poke;
            }
        })
        
        if(!p) {
            newPokemon._id = ++this.count;
            this.pokemons.push(newPokemon);
            //this.pokemons.sort((a, b) => (a.pokemonId > b.pokemonId) ? 1 : -1);

            resp.setData(newPokemon);
            resp.setSuccess(true);
            resp.setMessage('Pokemon added successfully');
            resp.setStatus(200);
        } else {
            resp.setSuccess(false);
            resp.setMessage("Same pokemonId already in the database");
            resp.setStatus(400);
        }

        return resp;
    }

    updatePokemon(id, data) {
        let pokemon = this.pokemons.find(pokemon => pokemon._id === id);
    
        const resp = new customResponse();

        if(pokemon) {
            pokemon.pokemonId = data.pokemonId;
            pokemon.name = data.name;
            pokemon.species = data.species;
            pokemon.height = data.height;
            pokemon.weight = data.weight;
            pokemon.abilities = data.abilities;
            pokemon.type = data.type;
            pokemon.sprites = data.sprites;
            pokemon.baseStats = data.baseStats;

            resp.setData(pokemon);
            resp.setSuccess(true);
            resp.setMessage('Pokemon is updated');
            resp.setStatus(200);
        } else {
            resp.setSuccess(false);
            resp.setMessage("Pokemon not found");
            resp.setStatus(404);
        }       

        return resp;
    }

    deletePokemon(id) {
        let pokemon;
        let index;
  
        this.pokemons.map((p, i) => {       
            if (p._id === id) {
                pokemon = p;
                index = i;       
            }
        });

        const resp = new customResponse();

        if(pokemon){
            this.pokemons.splice(index, 1);
            
            resp.setData(pokemon);
            resp.setSuccess(true);
            resp.setMessage('Pokemon is deleted');
            resp.setStatus(200);
        } else {
            resp.setSuccess(false);
            resp.setMessage('Pokemon not found');
            resp.setStatus(404);
        }

        return resp;
    }
}

module.exports = new PokemonProcessor();