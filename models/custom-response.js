class CustomResponse {
    constructor() {
        this.data = null;
        this.success = true;
        this.message = '';
        this.status = 404;
    }

    setData(data) {
        this.data = data;
    }

    setSuccess(success) {
        this.success = success;
    }

    setMessage(message) {
        this.message = message;
    }

    setStatus(status) {
        this.status = status;
    }

    getStatus() {
        return this.status;
    }
}

module.exports = CustomResponse;